import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  id("org.jetbrains.kotlin.jvm").version("1.3.10")
  id("com.github.johnrengelman.shadow") version "4.0.3"
  application
}

repositories {
  jcenter()
}

dependencies {
  implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
  implementation("io.vertx:vertx-core:3.6.1")
  implementation("io.vertx:vertx-lang-kotlin:3.6.1")
  implementation("io.cloudevents:http-vertx:0.2.0")
  implementation("com.github.ajalt:clikt:1.6.0")
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions.jvmTarget = "1.8"

application {
  mainClassName = "source.AppKt"
}

tasks.withType<ShadowJar> {
  classifier = "fat"
  mergeServiceFiles {
    include("META-INF/services/io.vertx.core.spi.VerticleFactory")
  }
}
