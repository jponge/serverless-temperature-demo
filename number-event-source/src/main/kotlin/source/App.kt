package source

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.long
import io.cloudevents.CloudEvent
import io.cloudevents.CloudEventBuilder
import io.cloudevents.http.vertx.VertxCloudEvents
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import java.net.URI
import java.net.URL

class NumberSource : CliktCommand() {

  val period by option().long().default(1000)
  val sink by option().required()

  var number: Long = 0

  override fun run() {
    val hostname = URL(sink).host
    val vertx = Vertx.vertx()
    val client = vertx.createHttpClient()
    val cloudEvents = VertxCloudEvents.create()

    echo("Starting pushing events to $hostname every $period milliseconds")
    while (true) {
      Thread.sleep(period)
      number += 1
      val payload = json {
        obj(
          "n" to number
        )
      }
      val event = CloudEventBuilder<JsonObject>()
        .type("io.foo.bar")
        .source(URI("/number"))
        .id(number.toString())
        .contentType("application/json")
        .data(payload)
        .build()
      val req = client.post(80, hostname, "/")
      req.handler { resp ->
        echo("Status ${resp.statusCode()} // $event")
      }
      cloudEvents.writeToHttpClientRequest(event, req)
      req.end()
    }
  }
}

fun main(args: Array<String>) = NumberSource().main(args)
