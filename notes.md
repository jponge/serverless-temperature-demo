## Talk to the knative serving ingress

echo $(minikube ip):$(kubectl get svc knative-ingressgateway --namespace istio-system --output 'jsonpath={.spec.ports[?(@.port==80)].nodePort}')

## Use local images

eval $(minikube docker-env)
kubectl config use-context minikube
export KO_DOCKER_REPO="ko.local"

Then use dev.local prefix and never tag :latest

eval $(minikube docker-env)
docker pull gcr.io/knative-samples/primer:latest
docker tag gcr.io/knative-samples/primer:latest dev.local/knative-samples/primer:v1

## Get a knative serving domain

kubectl get ksvc temp-ingest-function  --output=custom-columns=NAME:.metadata.name,DOMAIN:.status.domain

## Example function tag

dev.local/serverless-temperature-demo/ingest-function:v1

## Example request

http 192.168.64.26:32380 Host:temp-ingest-function.default.example.com temperature=21.1 sensorID=123

