package dumper

import io.vertx.core.AbstractVerticle
import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler

class Dumper : AbstractVerticle() {

  override fun start() {
    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())
    router.route().handler { ctx ->
      System.out.println("Method = ${ctx.request().method()}")
      System.out.println("Headers = ${ctx.request().headers()}")
      System.out.println("Body =\n ${ctx.body}")
      System.out.println("------------------------------------")
      ctx.response().end("Ok!")
    }

    vertx.createHttpServer()
      .requestHandler(router)
      .listen(8080)
  }
}

fun main(args: Array<String>) {
  Vertx.vertx().deployVerticle(Dumper())
}
