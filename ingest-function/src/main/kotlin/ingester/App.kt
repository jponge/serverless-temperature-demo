package ingester

import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpServerRequest
import io.vertx.ext.mongo.MongoClient
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import java.time.Instant

class Ingester : AbstractVerticle() {

  private val dbName = System.getenv("DB_NAME") ?: "mydb"
  private val dbHost = System.getenv("DB_HOST") ?: "localhost"
  private val dbPort = (System.getenv("DB_PORT") ?: "27017").toInt()

  private lateinit var db: MongoClient

  override fun start(startFuture: Future<Void>) {
    val mongoConf = json {
      obj (
        "host" to dbHost,
        "port" to dbPort,
        "db_name" to dbName,
        "useObjectId" to true
      )
    }
    db = MongoClient.createShared(vertx, mongoConf)

    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())
    router.post("/").handler(this::handleRequest)

    vertx.createHttpServer()
      .requestHandler(router)
      .listen(8080) {
        if (it.succeeded()) {
          startFuture.complete()
        } else {
          startFuture.fail(it.cause())
        }
      }
  }

  private fun handleRequest(ctx: RoutingContext) {
    val data = ctx.bodyAsJson
    if (data == null || !data.containsKey("temperature") || !data.containsKey("sensorID")) {
      ctx.fail(400)
    }
    val dbData = json {
      obj (
        "timestamp" to Instant.now().epochSecond,
        "sensorID" to data.getString("sensorID"),
        "temperature" to data.getString("temperature").toDouble()
      )
    }
    db.insert("temperatures", dbData) {
      if (it.succeeded()) {
        ctx.response().end()
      } else {
        it.cause().printStackTrace()
        ctx.fail(500)
      }
    }
  }
}

fun main(args: Array<String>) {
  Vertx.vertx().deployVerticle(Ingester())
}
